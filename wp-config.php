<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mangale' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Cp@{}GWHgZ:W:5el1K?pFxbTFE50w3j JRRNO^$/]OyMBKM$f[:]%$ra@,9;Efv=' );
define( 'SECURE_AUTH_KEY',  '-+y3lnXrH%@Z{f}62)fFf.sm_RB++=U8do(*@]qojc^L)LM9)@1UNb@EX~*>?:gW' );
define( 'LOGGED_IN_KEY',    '-f>_+!9qd18^Ag*aJ_IIs|=L`%gM}*#XKb4?}PWsW}JVch@V/$Zc#8 +#-6sG0S4' );
define( 'NONCE_KEY',        '.?:OH(_i2>%N9l3T[WG!v!sQ|[+ jn^A)f<^+hvGnJI.A%>42?v!b>!W|[|4!UL:' );
define( 'AUTH_SALT',        'aM0NS}VED,b,?ln}uaT^X53?[v>!7<TwQS1Fh/biw4w.9 kF],t}>93C}Y6T2AR%' );
define( 'SECURE_AUTH_SALT', ',j8tK #<lQ7DIct8G!hQN283(Yx2#ay3;(B0luL7fMq%+nW<O1]4;vYs;Pf/8iZ]' );
define( 'LOGGED_IN_SALT',   'AV@>JZ|a>}_E{!{IWyAsPIwqhE*[Pkphh0Vco]}Hs@YpG1S|6d(RFBW}ArOmhhwj' );
define( 'NONCE_SALT',       'RVmyY:.y`F(r*ok/wwsqL<awn~Z~nlP=UR@xu3Fo+RXr%y-Qrw,nmaC(o+[4Y7Bd' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
